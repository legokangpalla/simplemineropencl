﻿//
// MainPage.xaml.cpp
// Implementation of the MainPage class.
//

#include "pch.h"
#include "MainPage.xaml.h"
#include <ppltasks.h>
using namespace SimpleMinerUWP;

using namespace concurrency;
using namespace Platform;
using namespace Windows::ApplicationModel;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Metadata;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409


MainPage::MainPage()
{
	InitializeComponent();
}
void MainPage::setAppReference(WeakReference appWReference)
{
	_appReference = appWReference;
	App^ myApp = _appReference.Resolve<App>();
	if (myApp)
	{
		myApp->AppServiceConnected += ref new AppServiceConnectedHandler(this, &MainPage::appServiceConnected);
	}

	if (ApiInformation::IsApiContractPresent(L"Windows.ApplicationModel.FullTrustAppContract", 1, 0))
	{
		OutputDebugString(L"run full trust started");

		create_task(FullTrustProcessLauncher::LaunchFullTrustProcessForCurrentAppAsync()).
			then([](task<void> fullTrustLauncherTask) {
			try
			{
				fullTrustLauncherTask.get();
				// .get() didn' t throw, so we succeeded.
				OutputDebugString(L"run full trust works");
			}
			catch (COMException^ e)
			{
				OutputDebugString(L"run full trust failed");

				//Example output: The system cannot find the specified file.
				//OutputDebugString(e->Message->Data());
			}
		
		});
	}
}


void SimpleMinerUWP::MainPage::appServiceConnected(App ^ sender)
{
	//pass data to the full trust app;
}
