﻿//
// MainPage.xaml.h
// Declaration of the MainPage class.
//

#pragma once

#include "MainPage.g.h"

namespace SimpleMinerUWP
{
	/// <summary>
	/// An empty page that can be used on its own or navigated to within a Frame.
	/// </summary>
	public ref class MainPage sealed
	{
	public:
		MainPage();
	internal:
		void setAppReference(Platform::WeakReference appWReference);
	private:

		void appServiceConnected(App^ sender);

		Platform::WeakReference _appReference;
	};
}
