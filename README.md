# SimpleMinerOpenCL
This is a simple miner(for leading zero problems) with W32 and UWP UI.
It is hardware accelerated via OpenCL.
It implementes(for now) custom SHA1 OpenCL kernel for hashing and checking solutions.
Has been tested for CUDA version of OpenCL on Windows 10.
## Installation
Run the .sln file, build, and use the program GUI.
OpenCL only supports x64 platform(fails to find OpenCL devices on x86 build) so set your platform to x64.
## License
MIT