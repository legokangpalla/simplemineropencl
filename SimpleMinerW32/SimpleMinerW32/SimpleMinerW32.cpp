// SimpleMinerW32.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <windows.h>
#include "resource.h"
#include "MainDialogW32.h"



LRESULT __stdcall WindowProcedure(HWND window, unsigned int msg, WPARAM wp, LPARAM lp)

{

	switch (msg)

	{

	case WM_DESTROY:

		std::cout << "\ndestroying window\n";

		PostQuitMessage(0);

		return 0L;

	case WM_LBUTTONDOWN:
	{
		wchar_t szFileName[MAX_PATH];
		HINSTANCE hInstance = GetModuleHandle(NULL);

		GetModuleFileName(hInstance, szFileName, MAX_PATH);
		MessageBox(window, szFileName, L"This program is:", MB_OK | MB_ICONINFORMATION);
	}
	// fall thru
	case WM_CREATE:
	{
		HMENU hMenu, hSubMenu;
		HICON hIcon, hIconSm;

		hMenu = CreateMenu();

		hSubMenu = CreatePopupMenu();
		AppendMenu(hSubMenu, MF_STRING, ID_FILE_EXIT, L"E&xit");
		AppendMenu(hMenu, MF_STRING | MF_POPUP, (UINT)hSubMenu, L"&File");

		hSubMenu = CreatePopupMenu();
		AppendMenu(hSubMenu, MF_STRING, ID_STUFF_GO, L"&Go");
		AppendMenu(hMenu, MF_STRING | MF_POPUP, (UINT)hSubMenu, L"&Stuff");

		SetMenu(window, hMenu);


		hIcon = (HICON)LoadImage(NULL, L"icon1.ico", IMAGE_ICON, 32, 32, LR_LOADFROMFILE);
		if (hIcon)
			SendMessage(window, WM_SETICON, ICON_BIG, (LPARAM)hIcon);
		else
			MessageBox(window, L"Could not load large icon!", L"Error", MB_OK | MB_ICONERROR);

		hIconSm = (HICON)LoadImage(NULL, L"icon1.ico", IMAGE_ICON, 16, 16, LR_LOADFROMFILE);
		if (hIconSm)
			SendMessage(window, WM_SETICON, ICON_SMALL, (LPARAM)hIconSm);
		else
			MessageBox(window, L"Could not load small icon!", L"Error", MB_OK | MB_ICONERROR);
		break;
	}
	case WM_COMMAND:
	{
		//process button commands
		switch (LOWORD(wp))
		{
		default:
			break;
		}
	}
	default:

		//std::cout << '.';

		return DefWindowProc(window, msg, wp, lp);

	}

}
void startWindow()
{
	const wchar_t* const windowClassName = L"MinerMainWindowClass";

	WNDCLASSEX wndclass = { sizeof(WNDCLASSEX), CS_DBLCLKS, WindowProcedure,

							0, 0, GetModuleHandle(0), LoadIcon(0,IDI_APPLICATION),

							LoadCursor(0,IDC_ARROW), HBRUSH(COLOR_WINDOW + 1),

							0, windowClassName, LoadIcon(0,IDI_APPLICATION) };

	//setmenu
	//wndclass.lpszMenuName = MAKEINTRESOURCE(IDR_MYMENU);
	//wndclass.hIcon = LoadIcon(GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_MYICON));
	//wndclass.hIconSm = (HICON)LoadImage(GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_MYICON), IMAGE_ICON, 16, 16, 0);


	if (RegisterClassEx(&wndclass))
	{

		HWND window = CreateWindowEx(0, windowClassName, L"title",

			WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT,

			CW_USEDEFAULT, CW_USEDEFAULT, 0, 0, GetModuleHandle(0), 0);

		if (window)

		{

			ShowWindow(window, SW_SHOWDEFAULT);

			MSG msg;

			while (GetMessage(&msg, NULL, 0, 0))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}


		}

	}
	else
	{
		MessageBox(NULL, L"Window Registration Failed!", L"Error!",
			MB_ICONEXCLAMATION | MB_OK);
	}

}




//BOOL CALLBACK MaindDlgProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
//{
//	switch (Message)
//	{
//	case WM_INITDIALOG:
//
//		return TRUE;
//	case WM_COMMAND:
//		switch (LOWORD(wParam))
//		{
//		case IDOK:
//			EndDialog(hwnd, IDOK);
//			break;
//		}
//		break;
//	default:
//		return FALSE;
//	}
//	return TRUE;
//}


void startMainDialog()
{
	//DialogBox(GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_MAIN), hwnd, MaindDlgProc);



}


int main()
{

	MainDialogW32 mainDialog;
	mainDialog.openDialog();
}
