#include "pch.h"
#include "MainDialogW32.h"

//w32
#include "resource.h"
#include <commctrl.h>

//omg this makes my eyes water
//static
int __stdcall MainDialogW32::MaindDlgProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
	if (Message == WM_INITDIALOG)
	{
		SetProp(hwnd, L"this", (HANDLE)lParam);
	}
	MainDialogW32 *pThis = reinterpret_cast<MainDialogW32*>(GetProp(hwnd, L"this"));
	return pThis->MaindDlgProcImpl(hwnd, Message, wParam, lParam);

}
//unsorted, append
inline void setListBoxStrings(HWND hwnd, const std::vector<std::wstring>& strings)
{

	for (auto& each : strings)
	{
		SendMessage(hwnd, LB_ADDSTRING, NULL, (LPARAM)each.c_str());
	}
}
int __stdcall MainDialogW32::MaindDlgProcImpl(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{


	switch (Message)
	{
	case WM_INITDIALOG:	
	{
		//set spin control
		//set buddy
		HWND hwndBuddy = GetDlgItem(hwnd, IDC_DIFFICULTYVIEW);
		HWND hwndSpin = GetDlgItem(hwnd, IDC_DIFFICULTY);
		SendMessage(hwndSpin, UDM_SETBUDDY, (WPARAM)hwndBuddy, 0);
		SendMessage(hwndSpin, UDM_SETBASE, 10, 0);
		//get dialog handle
		_hDialog = hwnd;
		//Run stop buttons should be disabled from the start
		_isRunStopable = false;

		return TRUE;

	}
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDC_OPENCL:
		{
			SMDetectOpenCL();
			break;
		}

		case IDC_PLATFORMS:
		{
			if (HIWORD(wParam) == LBN_SELCHANGE)
			{
				// Get selected index.
				HWND hwndPlatforms = GetDlgItem(hwnd, IDC_PLATFORMS);
				int platformID = (int)SendMessage(hwndPlatforms, LB_GETCURSEL, 0, 0);
				SMSelectPlatform(platformID);
			}
			break;
		}

		case IDC_DEVICES:
		{
			if (HIWORD(wParam) == LBN_SELCHANGE)
			{
				// Get selected index.
				HWND hwndDevices = GetDlgItem(hwnd, IDC_DEVICES);
				int deviceID = (int)SendMessage(hwndDevices, LB_GETCURSEL, 0, 0);
				SMSelectDevice(deviceID);
			}
			break;
		}
		case IDC_HASHALGOS:
		{
			if (HIWORD(wParam) == LBN_SELCHANGE)
			{
				// Get selected index.
				HWND hwndHashs = GetDlgItem(hwnd, IDC_HASHALGOS);
				int hashID = (int)SendMessage(hwndHashs, LB_GETCURSEL, 0, 0);
				SMSelectHashAlgo(hashID);
			}
			break;
		}

		case IDC_DIFFICULTY:
		{
			break;
		}
		case IDRUN:
		{
			SMRun();
			break;

		}
		case IDSTOP:
		{
			SMStop();
			break;

		}
		}
		break;
	case WM_CLOSE:
	{
		DestroyWindow(hwnd);

	}
	default:
		return FALSE;
	}
	return TRUE;
}


void MainDialogW32::openDialog()
{
	
	DialogBoxParam(GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_MAINDIALOG), NULL, (DLGPROC)MaindDlgProc, (LPARAM)this);

}

//MainDialogW32::~MainDialogW32()
//{
//}

void MainDialogW32::showMessage(std::wstring msg)
{
	MessageBox(NULL, msg.c_str(), L"Message",
		MB_ICONEXCLAMATION | MB_OK);
}

void MainDialogW32::updatePlatforms(std::vector<std::wstring> platformNames)
{
	HWND hwndList = GetDlgItem(_hDialog, IDC_PLATFORMS);
	setListBoxStrings(hwndList, platformNames);
}

void MainDialogW32::updateDevices(std::vector<std::wstring> deviceNames)
{
	HWND hwndList = GetDlgItem(_hDialog, IDC_DEVICES);
	setListBoxStrings(hwndList, deviceNames);
}

void MainDialogW32::updateHashAlgos(std::vector<std::wstring> hashAlgoNames)
{
	HWND hwndList = GetDlgItem(_hDialog, IDC_HASHALGOS);
	setListBoxStrings(hwndList, hashAlgoNames);
}

void MainDialogW32::updateMaxDifficulty(size_t maxDifficulty)
{
	//set possible difficulties, difficulty 0 being default
	HWND hwndSpin = GetDlgItem(_hDialog, IDC_DIFFICULTY);
	SendMessage(hwndSpin, UDM_SETRANGE, 0, MAKELPARAM(maxDifficulty, 0));
	SendMessage(hwndSpin, UDM_SETPOS, 0, 0);
}

void MainDialogW32::updateRunCancel(bool isEnabled)
{
	if (isEnabled != _isRunStopable)
	{
		HWND hwndRun = GetDlgItem(_hDialog, IDRUN);
		HWND hwndStop = GetDlgItem(_hDialog, IDSTOP);
		EnableWindow(hwndRun, isEnabled);
		EnableWindow(hwndStop, isEnabled);
	}
}

size_t MainDialogW32::getDifficulty()
{
	HWND hwndSpin = GetDlgItem(_hDialog, IDC_DIFFICULTY);
	LRESULT result = SendMessage(hwndSpin, UDM_GETPOS, 0, 0);
	return LOWORD(result);
}

std::wstring MainDialogW32::getPrefix()
{
	HWND hwndEdit = GetDlgItem(_hDialog, IDC_PREFIX);
	size_t length = GetWindowTextLength(hwndEdit);
	//null char
	wchar_t* buff = new wchar_t[length+1];
	GetWindowText(hwndEdit, buff, length + 1);
	std::wstring result(buff, length);
	delete[] buff;
	return result;
}
void MainDialogW32::updateResult(std::wstring result)
{
	HWND hwndEdit = GetDlgItem(_hDialog, IDC_RESULT);
	SetWindowText(hwndEdit, result.c_str());
}
