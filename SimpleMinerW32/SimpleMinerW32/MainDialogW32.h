#pragma once
#include "../../Common/MainDialog.h"
#include <windows.h>

class MainDialogW32 : private MainDialog
{
public:
	void openDialog()override;
	//virtual ~MainDialogW32();
protected:
	void showMessage(std::wstring msg)override;
	//UI related
	void updatePlatforms(std::vector<std::wstring> platformNames)override;
	void updateDevices(std::vector<std::wstring> deviceNames)override;
	void updateHashAlgos(std::vector<std::wstring> hashAlgoNames)override;
	void updateMaxDifficulty(size_t maxDifficulty)override;
	void updateRunCancel(bool isEnabled)override;
	void updateResult(std::wstring result)override;

	size_t getDifficulty()override;
	std::wstring getPrefix()override;
private:
	int __stdcall MaindDlgProcImpl(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam);
	HWND _hDialog;
	bool _isRunStopable;

	static int __stdcall MaindDlgProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam);


};