//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by SimpleMinerW32.rc
//
#define IDR_MENU1                       101
#define IDI_MYICON                      101
#define IDR_MYMENU                      101
#define IDR_MAINMENU                    101
#define IDI_ICON1                       103
#define IDD_MAINDIALOG                  104
#define IDC_PLATFORMS                   1001
#define IDC_OPENCL                      1002
#define IDC_DEVICES                     1004
#define IDC_HASHALGOS                   1005
#define IDC_PREFIX                      1007
#define IDRUN                           1008
#define IDSTOP                          1009
#define IDC_DIFFICULTY                  1010
#define IDC_DIFFICULTYVIEW              1011
#define IDC_PREFIX2                     1012
#define IDC_RESULT                      1012
#define IDC_RESULTHEADER                1013
#define ID_FILE_EXIT                    9001
#define ID_STUFF_GO                     9002

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        106
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1014
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
