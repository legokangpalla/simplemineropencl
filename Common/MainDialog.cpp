#include "pch.h"
#include "MainDialog.h"
#include "Util.h"
//opencl
#include <CL/cl.hpp>

const std::unordered_map<std::wstring, std::type_index> MainDialog::__hashNames = { {L"SHA1", std::type_index(typeid(AttackerSHA1))} };

//MainDialog::~MainDialog()
//{
//}

std::vector<std::wstring> MainDialog::getPlatforms()
{
	//get platform
	std::vector<std::wstring> platformNames;
	cl::Platform::get(&_platforms);
	if (_platforms.size() == 0) {

		showMessage(L" No platforms found. Check OpenCL installation!");
		return platformNames;
	}
	//names in wchar
	for (auto& platform : _platforms)
	{
		std::string platformName = platform.getInfo<CL_PLATFORM_NAME>();
		platformNames.push_back(Util::toWstr(platformName));

	}
	return platformNames;
}


 std::vector<std::wstring> MainDialog::getDevices(size_t platformIndex)
{
	 std::vector<std::wstring> deviceNames;
	 auto platform = _platforms[platformIndex];
	 platform.getDevices(CL_DEVICE_TYPE_ALL, &_devices);

	 if (_devices.size() == 0) {

		 showMessage(L" No devices found!");
		 return deviceNames;
	 }
	 for (auto &device : _devices)
	 {
		 std::string name = device.getInfo<CL_DEVICE_NAME>();
		 deviceNames.push_back(Util::toWstr(name));

	 }
	 return deviceNames;
}



 std::vector<std::wstring> MainDialog::getHashAlgorithms()
 {
	 //only one hashing algo atm
	 std::vector<std::wstring> hasherNames;
	 for (auto& each : __hashNames)
	 {
		 hasherNames.push_back(each.first);
		 _hashAlgos.push_back(each.second);
	 }
	 return hasherNames;
 }

 size_t MainDialog::getMaxDifficulty()
 {
	 return _attacker->digestLength();
 }

 void MainDialog::setRunStopButtons()
 {
	 switch (_currentState)
	 {
	 case State::Begin:
	 case State::OpenCLDetected:
	 case State::PlatformSelected:
	 case State::DeviceSelected:
	 {
		 updateRunCancel(false);
		 break;
	 }
	 case State::HashAlgoSelected:
	 default:
	 {
		 updateRunCancel(true);
	 }
	 }
	 return;
 }

 void MainDialog::SMDetectOpenCL()
 {
	 updatePlatforms(getPlatforms());
	 _currentState = State::OpenCLDetected;
	 setRunStopButtons();
 }

 void MainDialog::SMSelectPlatform(size_t platformID)
 {
	 updateDevices(getDevices(platformID));
	 _currentState = State::PlatformSelected;
	 setRunStopButtons();
 }

 void MainDialog::SMSelectDevice(size_t deviceID)
 {
	 _selectedDevice = _devices[deviceID];
	 updateHashAlgos(getHashAlgorithms());
	 _currentState = State::DeviceSelected;
	 setRunStopButtons();

 }

 void MainDialog::SMSelectHashAlgo(size_t hashAlgoID)
 {
	 //set attacker based on hash algorithm
	 auto hashClassID = _hashAlgos[hashAlgoID];
	 if (hashClassID == std::type_index(typeid(AttackerSHA1)))
	 {
		 _attackerValue = AttackerSHA1();
		 _attacker = &std::get<AttackerSHA1>(_attackerValue);
	 }
	 //else if()
	 //{
		// ...
	 //}
	 updateMaxDifficulty(getMaxDifficulty());
	 _currentState = State::HashAlgoSelected;
	 //finally ready to run, other options have default values so they don't have to be set
	 setRunStopButtons();
 }

 void MainDialog::SMRun()
 {
	 std::string prefix = Util::toSstr(getPrefix());
	 std::string result = _attacker->attack(_selectedDevice, prefix, getDifficulty());
	 updateResult(Util::toWstr(result));

 }

 void MainDialog::SMStop()
 {
	 _attacker->cancel();
 }
