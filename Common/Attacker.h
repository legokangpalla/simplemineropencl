#pragma once
#include <string>
#include <vector>
#include <CL/cl.hpp>
class Attacker
{
public:
	Attacker();

	//blocking operation
	virtual std::string attack(cl::Device device, std::string prefix, size_t difficulty) = 0;
	virtual void cancel() = 0;
	//when viewed as hex digits
	virtual size_t digestLength() = 0;
	virtual ~Attacker();

};