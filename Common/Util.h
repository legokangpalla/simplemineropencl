#pragma once
#include <string>
namespace Util
{
	//for ASCII/single byte char only...
	std::wstring inline toWstr(const std::string& str)
	{
		std::wstring ws;
		ws.assign(str.begin(), str.end());
		return ws;
	}
	//for ASCII/single byte char only...
	std::string inline toSstr(const std::wstring& str)
	{
		std::string ss;
		ss.assign(str.begin(), str.end());
		return ss;
	}
}