#pragma once
#include <vector>
#include <string>
#include <CL/cl.hpp>
#include <variant>
#include "AttackerSHA1.h"
#include <unordered_map>
#include <typeindex>



class MainDialog
{
	enum HashType
	{
		SHA1 = 0
	};
	enum State
	{
		Begin = 0,
		OpenCLDetected,
		PlatformSelected,
		DeviceSelected,
		HashAlgoSelected,
		Started,
		Finished
	};
public:
	virtual void openDialog() = 0;
	//virtual ~MainDialog();
protected:
	//message box
	virtual void showMessage(std::wstring msg) = 0;

	//state machine actions
	//void doState();
	void SMDetectOpenCL();
	void SMSelectPlatform(size_t platformID);
	void SMSelectDevice(size_t deviceID);
	void SMSelectHashAlgo(size_t hashAlgoID);
	void SMRun();
	void SMStop();
	//state machine helpers
	std::vector<std::wstring> getPlatforms();
	std::vector<std::wstring> getDevices(size_t platformIndex);
	std::vector<std::wstring> getHashAlgorithms();
	size_t getMaxDifficulty();
	void setRunStopButtons();
	//UI related
	virtual void updatePlatforms(std::vector<std::wstring> platformNames) = 0;
	virtual void updateDevices(std::vector<std::wstring> deviceNames) = 0;
	virtual void updateHashAlgos(std::vector<std::wstring> hashAlgoNames) = 0;
	virtual void updateMaxDifficulty(size_t maxDifficulty) = 0;
	virtual void updateRunCancel(bool isEnabled) = 0;
	virtual void updateResult(std::wstring result) = 0;

	virtual size_t getDifficulty() = 0;
	virtual std::wstring getPrefix() = 0;
	




private:


	std::vector<cl::Platform> _platforms;
	std::vector<cl::Device> _devices;
	std::vector<std::type_index> _hashAlgos;
	cl::Device _selectedDevice;


	//experimenting with variant for stack based polymmorphism.
	std::variant< AttackerSHA1> _attackerValue;
	//this is just to get around "get" boilerplates
	AttackerSHA1* _attacker;
	static const std::unordered_map<std::wstring, std::type_index> __hashNames;
	State _currentState;

};