#pragma once
#include "Attacker.h"

class AttackerSHA1 : public Attacker
{
public:
	using Attacker::Attacker;
	std::string attack(cl::Device device, std::string prefix, size_t difficulty)override;
	void cancel()override;
	size_t digestLength()override;

	virtual ~AttackerSHA1();

private:
};