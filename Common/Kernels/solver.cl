//#pragma OPENCL EXTENSION extension_name : behavior
#pragma OPENCL EXTENSION cl_nv_pragma_unroll : enable


//#include "Attacker/Kernels/utility.cl"
//#include "Attacker/Kernels/RandomCL/tinymt32.cl"
#include "../../Common/Kernels/sha1/sha1.cl"

//not sure how expensive mem fencing is for global variables, but don't want to check
//exit condition each iteration, so do it once a while
//also to save random operation, keeps same string length for the duration
#define CHECK_DURATION 30000
#define PREFIX_MAX 300
#define SUFFIX_MIN 1
#define SUFFIX_MAX 10
#define STR_MAX (PREFIX_MAX + SUFFIX_MAX)
#define UINT32_MAX 0xFFFFFFFF
#define WORK_COUNT 200




//at start pFinished is always false, set true if one of work item finds solution
void kernel attackCL(global uchar* pOutSolution, global uint* pSolutionLength, global volatile  int* pFinished, global const uint* pPrefixLength, global const uchar* pPrefix , global const uint* pDifficulty)
{
	if (*pDifficulty == 0)
		return;
	uint initialChar;
	bool wasInvalid, isInvalid;
	uint suffixLength = 0;
	uint tmpSuffixLength = 0;
	bool isSuccess = false;
	int alreadyFinished;
	uint currentPower;
	uint currentDigit; 
	uint carry;
	uint prefixLength = *pPrefixLength;
	uint difficulty = *pDifficulty;
	//whole string = prefix + suffix
	uchar whole[STR_MAX] = { 0 };
	uint wholeLength;

	//copy prefix to whole string
	for (uint i = 0; i < prefixLength; ++i)
	{
		whole[i] = pPrefix[i] - CHAR_OFFSET;
	}

	//ID is used as initial char
	initialChar = get_global_id(0);
	uint initialCarry = initialChar / STRING_BASE;
	if (initialCarry > 0)
	{
		initialChar = initialChar % STRING_BASE;
		//under small work count, it's safe to say there won't be multiple carrys... possible buf if not
		whole[prefixLength + 1] = initialCarry;
		++suffixLength;
	}
	whole[prefixLength] = (uchar)initialChar;
	++suffixLength;

	//main attack loop
	while (true)
	{
		for (uint checkRate = 0; checkRate < CHECK_DURATION; ++checkRate)
		{
			wholeLength = prefixLength + suffixLength;
			//check hash
			if (sha1HashCheck(prefixLength + suffixLength, whole, difficulty))
			{
				//check if other thread found the solution
				alreadyFinished = atomic_xchg(pFinished, 1);//Read the atomic value, get the old value,
				if (alreadyFinished)
					return;
				printf("a compute core found solution!\n");

				for (int j = 0; j < suffixLength; ++j)
				{
					//we need to add back so that range becomes 0 ~ (STRING_BASE-1) to CHAR_OFFSET ~ 127
					pOutSolution[j] = whole[j + prefixLength] + CHAR_OFFSET;
				}
				*pSolutionLength = suffixLength;
				return;
			}
			//get new string
			//get lowest digit, left most character in suffix
			currentPower = prefixLength;
			currentDigit = whole[currentPower];
			//do carrys
			//increment, since there are WORK_COUNT many workers, increment by WORK_COUNT to avoid overlapping
			carry = WORK_COUNT;
			do
			{
				//uint debugPre, debugPost;
				//debugPre = currentDigit;
				//check suffix length
				tmpSuffixLength = currentPower - prefixLength + 1;
				if (tmpSuffixLength > suffixLength)
				{
					//set suffixLength if it's a new record
					suffixLength = tmpSuffixLength;
					if (suffixLength > PREFIX_MAX)
					{
						printf("reached maximum explorable region, exiting...  \n");
						return;
					}
				}
				//add carry
				currentDigit += carry;
				//get next carry
				carry = currentDigit / STRING_BASE;
				//get remainder
				currentDigit = currentDigit % STRING_BASE;
				//apply leftovers to current digit
				whole[currentPower] = currentDigit;

				//go to next higher digit(power) since this is little-endian(first character in suffix is smallest power) +1
				++currentPower;
				//get next digit value
				currentDigit = whole[currentPower];
				//printf("    current invalid count%d\n", invalidCharCount);

			} while (carry != 0);
			
		}
		//check if other thread found the solution
		//there is no read mutex on finishedFlag
		alreadyFinished = *pFinished;
		if (alreadyFinished)
			return;
	}




	//uint debugMax = 4000000000;
	//uint debugCurrent = 0;
	////main attack loop
	//while (true)
	//{
	//	//calculate random length of suffix
	//	float lengthMult = (float)tinymt32_uint(state) / (float)UINT32_MAX;
	//	uint suffixLength = (uint)(SUFFIX_MAX * lengthMult);
	//	if (suffixLength == 0)
	//		suffixLength = SUFFIX_MIN;
	//	for (uint i = 0; i < CHECK_DURATION; ++i)
	//	{
	//		++debugCurrent;
	//		if (debugCurrent == debugMax)
	//		{
	//			printf("timeout   \n");
	//			return;

	//		}
	//		uint suffixIndex = *pPrefixLength;
	//		for (uint j = 0; j < suffixLength;)
	//		{
	//			uint random;
	//			uchar bytes[4];
	//			//get uint random, which can be split to 4 random bytes.
	//			random = tinymt32_uint(state);
	//			splitUInt(random, bytes);
	//			//since length can be non-multiple of 4
	//			for (uint k = 0; k < 4 && j < suffixLength; ++k)
	//			{
	//				uchar generatedChar = bytes[k];
	//				//ascii range or UTF8 single byte character range. Don't want to deal with multibyte utf8 for performance reasons.
	//				generatedChar = generatedChar / 2;
	//				excludeWhitespace(&generatedChar);
	//				whole[suffixIndex] = generatedChar;
	//				++suffixIndex;
	//				++j;
	//			}
	//		}
	//		bool isSuccess = sha1HashCheck(*pPrefixLength + suffixLength, whole, *pDifficulty);
	//		//bool isSuccess = true;

	//		if (isSuccess)
	//		{

	//			//check if other thread found the solution
	//			alreadyFinished = atomic_xchg(pFinished, 1);//Read the atomic value, get the old value,
	//			if (alreadyFinished)
	//				return;
	//			//alreadyFinished = *pFinished;
	//	
	//			//*pFinished = 1;
	//			printf("a compute core found solution   \n");

	//			for (int j = 0; j < suffixLength; ++j)
	//			{
	//				pOutSolution[j] = whole[j + *pPrefixLength];
	//			}
	//			*pSolutionLength = suffixLength;
	//			return;
	//		}
	//		


	//	}

	//	//check if other thread found the solution
	//	//there is no read mutex on finishedFlag
	//	alreadyFinished = *pFinished;
	//	if (alreadyFinished)
	//		return;

	//}
}