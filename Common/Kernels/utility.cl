
#define DIGEST_INT32_SIZE 5
#define DIGEST_BYTE_SIZE 20
#define DIGEST_HEX_SIZE 40

//range of correct char values, so removing whitespace and control chars, 33~127
#define CHAR_OFFSET 33
#define SINGLE_BYTE_BASE 128
//maximum value for a character in suffix
#define STRING_BASE 95// (SINGLE_BYTE_BASE - CHAR_OFFSET)



void inline adjustWhitespaceChar(uchar* pCodepoint)
{
	if (*pCodepoint >= 0x9)
		*pCodepoint += 2;
	if (*pCodepoint >= 0xD)
		*pCodepoint += 1;
	if (*pCodepoint >= 0x20)
		*pCodepoint += 1;
}

bool inline isControlOrWhitespace(uchar pCodepoint)
{
	if (pCodepoint < 33)
		return true;
	return false;
}

bool inline isWhitespace(uchar pCodepoint)
{
	if (pCodepoint == 0x9)
		return true;
	if (pCodepoint == 0xA)
		return true;
	if (pCodepoint == 0xD)
		return true;
	if (pCodepoint == 0x20)
		return true;
	return false;
}

void inline splitUInt(uint pSource, uchar* pDestination)
{
	pDestination[0] = (pSource >> 24) & 0xFF;
	pDestination[1] = (pSource >> 16) & 0xFF;
	pDestination[2] = (pSource >> 8) & 0xFF;
	pDestination[3] = pSource & 0xFF;
}


//we bascially know that the hex string will be 40 char long
void inline  byteToHex(uchar pDigest, uchar* pHex)
{
	const uchar HEX_CHARACTERS[] = "0123456789abcdef";
	pHex[0] = HEX_CHARACTERS[(pDigest >> 4) & 0xF];
	pHex[1] = HEX_CHARACTERS[pDigest & 0xF];

}
