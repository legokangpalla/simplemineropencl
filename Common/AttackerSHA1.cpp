#include "pch.h"
#include "AttackerSHA1.h"

#include <iostream>
#include <fstream>
#include <chrono>
//opencl wrapper
#include <CL/cl.hpp>


//change this for different devices...
#define KERNEL_SOURCE_FILE "../../Common/Kernels/solver.cl"
#define PREFIX_MAX 300 //usually 64, but let's not take any chances
#define SUFFIX_MIN 1
#define SUFFIX_MAX 10
#define STR_MAX (PREFIX_MAX + SUFFIX_MAX)
#define WORK_COUNT 20
inline unsigned int to_uint(char ch)
{
	// EDIT: multi-cast fix as per David Hammen's comment
	return static_cast<unsigned int>(static_cast<unsigned char>(ch));
}
//wasn't sure how boost::compute handles kernel variables, so I wrote my own.
std::string inline defineKernelConstant(const std::string& pName, const std::string& pValue)
{
	std::string define = "#define " + pName + " (" + pValue + ")\n";
	return define;
}


std::string AttackerSHA1::attack(cl::Device device, std::string prefix, size_t difficulty)
{
	//check empty suffix
	//std::string onlyPrefix = boostHash(prefix);
	//size_t prefixHashZeroCount = 0;
	//for (size_t i = 0; i < difficulty; ++i)
	//{
	//	if (onlyPrefix[i] == '0')
	//	{
	//		++prefixHashZeroCount;
	//	}
	//	else
	//	{
	//		break;
	//	}
	//}
	//if (prefixHashZeroCount == difficulty)
	//	return "";

	std::string suffix;


	cl::Context context(device);
	cl::CommandQueue queue(context, device);

	//build kernel
	std::ifstream sourceFile(KERNEL_SOURCE_FILE);
	std::string sourceCode(
		std::istreambuf_iterator<char>(sourceFile),
		(std::istreambuf_iterator<char>()));
	cl::Program::Sources source(1, std::make_pair(sourceCode.c_str(), sourceCode.length() + 1));
	cl::Program program(context, source);
	try
	{
		program.build();
	}
	catch (cl::Error& e)
	{
		if (e.err() == CL_BUILD_PROGRAM_FAILURE)
		{

			// Check the build status
			cl_build_status status = program.getBuildInfo<CL_PROGRAM_BUILD_STATUS>(device);
			if (status == CL_BUILD_ERROR)
			{
				// Get the build log
				std::string name = device.getInfo<CL_DEVICE_NAME>();
				std::string buildlog = program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device);
				std::cerr << "Build log for " << name << ":" << std::endl
					<< buildlog << std::endl;
				return "";
			}
		}
		else
		{
			throw e;
		}
	}

	cl::Kernel kernel(program, "attackCL");

	//void kernel attackCL(global uchar* pOutSolution, global uint* pSolutionLength, global int* pFinished, global const uint* pPrefixLength, global const uchar* pPrefix , global const uint* pDifficulty)

	//host
	//these 4 are shared over all threads, since only 1 solution needs to be found
	//1 buffer for solution
	unsigned char solution[SUFFIX_MAX]{ 0 };
	//1 buffer for solution length
	uint32_t solutionLength[1]{0};
	// 1 int for signalling solution is found and early exit
	int isFinished[1]{ 0 };
	//length of the prefixRaw
	uint32_t prefixLength[1]{prefix.length()};
	//prefixRaw
	char prefixRaw[STR_MAX] {0};
	memcpy_s(prefixRaw, STR_MAX, prefix.c_str(), prefix.length());
	//difficultyRaw
	uint32_t difficultyRaw[1]{ difficulty };

	//device
	cl::Buffer bufSolution(context, CL_MEM_READ_WRITE, sizeof(unsigned char) * SUFFIX_MAX);
	cl::Buffer bufSolutionLength(context, CL_MEM_READ_WRITE, sizeof(uint32_t));
	cl::Buffer bufIsFinished(context, CL_MEM_READ_WRITE, sizeof(int));
	cl::Buffer bufPrefixLength(context, CL_MEM_READ_ONLY, sizeof(uint32_t));
	cl::Buffer bufPrefix(context, CL_MEM_READ_ONLY, sizeof(char)*STR_MAX);
	cl::Buffer bufDifficulty(context, CL_MEM_READ_ONLY, sizeof(uint32_t));

	//write to device

	queue.enqueueWriteBuffer(bufSolution, CL_TRUE, 0, sizeof(unsigned char)*SUFFIX_MAX, solution);
	queue.enqueueWriteBuffer(bufSolutionLength, CL_TRUE, 0, sizeof(uint32_t), solutionLength);
	queue.enqueueWriteBuffer(bufIsFinished, CL_TRUE, 0, sizeof(int), isFinished);
	queue.enqueueWriteBuffer(bufPrefixLength, CL_TRUE, 0, sizeof(uint32_t), prefixLength);
	queue.enqueueWriteBuffer(bufPrefix, CL_TRUE, 0, sizeof(char)*STR_MAX, prefixRaw);
	queue.enqueueWriteBuffer(bufDifficulty, CL_TRUE, 0, sizeof(uint32_t), difficultyRaw);

	//queue.enqueueWriteBuffer(buffer_N, CL_TRUE, 0, sizeof(int), N);


	//set kernel arguments
	kernel.setArg(0, bufSolution);
	kernel.setArg(1, bufSolutionLength);
	kernel.setArg(2, bufIsFinished);
	kernel.setArg(3, bufPrefixLength);
	kernel.setArg(4, bufPrefix);
	kernel.setArg(5, bufDifficulty);

	auto startTime = std::chrono::system_clock::now();
	//run kernel
	cl::NDRange global(WORK_COUNT);
	cl::NDRange local(1);

	queue.enqueueNDRangeKernel(kernel, 0, global, local);
	queue.finish();

	//read results
	queue.enqueueReadBuffer(bufSolution, CL_TRUE, 0, sizeof(unsigned char)* SUFFIX_MAX, solution);
	queue.enqueueReadBuffer(bufSolutionLength, CL_TRUE, 0, sizeof(uint32_t), solutionLength);
	auto endTime = std::chrono::system_clock::now();
	int elapsedMS = std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime).count();

	suffix = std::string(reinterpret_cast<char*>(solution), *solutionLength);
	//double check against boost
	std::string whole = prefix + suffix;
	//std::string str2 = boostHash(whole);

	std::cout << "Finished! took " << elapsedMS <<"ms" << std::endl;
	//std::cout << "Hash: " << str2 << std::endl;
	std::cout << "solution in string: " << suffix <<std::endl;

	std::cout << "solution in bytes: ";
	for (char ch : suffix)
	{
		std::cout << to_uint(ch) << ' ';
	}
	std::cout << std::endl;
	return suffix;
}

void AttackerSHA1::cancel()
{
	std::cout << "Cancel not implemented yet" << std::endl;
}

size_t AttackerSHA1::digestLength()
{
	return 40;
}

AttackerSHA1::~AttackerSHA1()
{
}
